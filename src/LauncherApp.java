import org.academiadecodigo.javabank.application.BankApp;

public class LauncherApp {
    public static void main(String[] args) {
        BankApp app = new BankApp();
        app.start();
    }
}
